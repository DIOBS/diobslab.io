#!/usr/bin/env bash

for file in $(find ./images -type f -name "*.svg"); do
    input="${file}"
    output="opt.svg"

    scour -i "${input}" -o "${output}" --enable-viewboxing --enable-id-stripping --enable-comment-stripping --shorten-ids --indent=none --disable-simplify-colors --disable-style-to-xml

    mv "opt.svg" "${input}"
done
